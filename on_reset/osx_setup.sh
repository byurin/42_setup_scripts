RESOURCES="$HOME/osx_setup/on_reset/resources"
BACKGROUND="$HOME/Pictures/my_background.jpg"

cp $RESOURCES/my_background.jpg $HOME/Pictures/

osascript -e "set desktopImage to POSIX file \"$BACKGROUND\"
tell application \"Finder\"
    set desktop picture to desktopImage
end tell"

cp $RESOURCES/com.apple.dock.plist ~/Library/Preferences

osascript -e "tell application \"System Events\"
	tell appearance preferences
		set properties to {dark mode:true, scroll bar action:jump to here}
	end tell
end tell"

defaults write com.apple.screensaver askForPassword -int 1

killall Dock

rm -ri $HOME/osx_setup

osascript -e "tell application \"System Events\" to log out"
